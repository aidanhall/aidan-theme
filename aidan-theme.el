;;; aidan-theme.el --- A slight tweak to the default theme according to my tastes. -*- lexical-binding: t; -*-

;; Copyright © 2022 Aidan Hall.

;; Author: Aidan Hall <aidan.hall@gmail.com>
;; Version: 0.0.2
;; URL: https://gitlab.com/aidanhall/aidan-theme
;; Keywords: faces theme

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.


;;; Commentary:

;; This was created by extracting my face customisations out of init.el.
;; It's mostly the same as the default, with the only notable change being
;; background colours for `display-line-numbers-mode'.

;;; Code:
(deftheme aidan
  "A slight tweak to the default theme according to my tastes.")

(custom-theme-set-variables
 'aidan
 )

(custom-theme-set-faces
 'aidan
 '(default ((t (:height 103 :foundry "ADBO" :family "Source Code Pro"))))
 '(line-number ((t (:inherit (shadow default) :background "white smoke"))))
 '(line-number-current-line ((t (:inherit line-number :inverse-video t))))
 '(org-document-title ((t (:inherit variable-pitch :foreground "midnight blue" :weight bold :height 1.5))))
 '(org-verbatim ((t (:inherit nil :weight semi-bold :family "Courier New"))))
 '(region ((t (:extend t :background "pink1" :distant-foreground "gtk_selection_fg_color")))))

(provide-theme 'aidan)

;;; aidan-theme.el ends here
